<%-- 
    Document   : Index
    Created on : Mar 6, 2018, 5:34:59 AM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <small>Welcome,</small>
        <a href="ViewProfile">${exist_acc}</a>
        <c:if test="${is_admin eq true}">
            (ADMIN)
        </c:if>

        <c:if test="${is_admin eq true}">
            <a href="AddUser.jsp">
                Create user
            </a>
        </c:if>
        <a href="Logout">
            Logout
        </a><br/>

        <form action="Post" method="post" enctype="multipart/form-data">

            <span class="input-icon">
                <input type="text" name="new_post"/>
                <button type="submit">Post</button>
            </span>
            <input name="photo" type="file">

        </form>
        <br/>   
        <jsp:useBean class="com.webapp.model.CommentManagerBean" id="commentBean" scope="page" />
        <c:forEach items="${posts}" var="post">
            <div style="border: 1px #333333 dashed; width: 500px; height: auto; padding: 10px 20px;">

                ${post.username}: ${post.content}<br/>

                <c:if test="${not empty post.picture}">
                    <img height="150" width="200" src="images/posts/${post.picture}"/><br/>
                </c:if>
                <jsp:setProperty name="commentBean"
                                 property="post_id" value="${post.id}"                                                     
                                 />  
                <c:choose>
                    <c:when test="${not empty commentBean.commentsByPostID}">
                        <c:forEach items="${commentBean.commentsByPostID}" var="comment">
                            ${comment.username}: ${comment.content}<br/>
                            <c:if test="${not empty comment.picture}">
                                <img height="120" width="160" src="images/comments/${comment.picture}"/><br/>
                            </c:if>
                            <form action="Comment" method="post" enctype="multipart/form-data">
                                <input name="post_id" value="${post.id}" hidden/>
                                <input type="text" name="new_comment"/>
                                <button type="submit">Send</button>
                                <input name="photo" type="file">
                            </form>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <form action="Comment" method="post" enctype="multipart/form-data">
                            <input name="post_id" value="${post.id}" hidden/>
                            <input type="text" name="new_comment"/>
                            <button type="submit">Send</button>
                            <input name="photo" type="file">
                        </form>
                    </c:otherwise>
                </c:choose>

            </div>
            <br/>
        </c:forEach>

    </body>
</html>
