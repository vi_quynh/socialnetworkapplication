<%-- 
    Document   : AddUser
    Created on : Mar 6, 2018, 5:00:59 AM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Welcome,</h3>
        <a href="ViewProfile">${exist_acc}</a>
    <c:if test="${is_admin eq true}">
        (ADMIN)
    </c:if>

    <c:if test="${is_admin eq true}">
        <a href="AddUser.jsp">
            Create user
        </a>
    </c:if>
    <a href="Logout">
        Logout
    </a><br/>
    <br/>
    <form action="AddUser" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td>Account</td>
                <td><input name="account" /></td>
            </tr>                  
            <tr>
                <td>Password </td>
                <td><input name="password"  type="password"/></td>
            </tr>
            <tr>
                <td>Your name</td>
                <td><input name="name"/></td>
            </tr>
            <tr>
                <td>Age</td>
                <td><input name="age" /></td>
            </tr>
            <tr>
                <td>Avatar</td>
                <td><input name="photo" type="file"/></td>
            </tr>
            <tr>
                <td colspan="10">
                    <button type="submit">Register</button>
                </td>
            </tr>
        </table>
    </form>
    <br/>
    ${msg_add}
</body>
</html>
