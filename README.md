# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
###SQL Server query###
CREATE DATABASE SocialNetWorkApplication
go
USE SocialNetWorkApplication
GO
CREATE TABLE dboUSER
(
	username VARCHAR(50) PRIMARY KEY,
	password VARCHAR(50),
	avatar VARCHAR(250),
	name VARCHAR(250),
	age INT,
	role VARCHAR(20) DEFAULT 'USER'
)
GO
CREATE TABLE dboPOST
(
	id INT IDENTITY(1,1) PRIMARY KEY,
	username VARCHAR(50) FOREIGN KEY REFERENCES dbo.dboUSER(username),
	content NTEXT,
	picture VARCHAR(250),
	date_create DATETIME DEFAULT GETDATE() 
)
GO
CREATE TABLE dboCOMMENT
(
	id  INT IDENTITY(1,1) PRIMARY KEY,
	username  VARCHAR(50) FOREIGN KEY REFERENCES dbo.dboUSER(username),
	postid INT FOREIGN KEY REFERENCES dbo.dboPOST(id),
	content NTEXT,
	picture VARCHAR(250),
	date_create DATETIME DEFAULT GETDATE()
)