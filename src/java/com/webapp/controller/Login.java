/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webapp.controller;

import com.webapp.entities.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.webapp.model.UserManagerBean;

/**
 *
 * @author viquy
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String acc = request.getParameter("acc");
        String pass = request.getParameter("pass");

        User user = new User();
        user.setUsername(acc);
        user.setPassword(pass);

        UserManagerBean managerBean = new UserManagerBean();
        managerBean.setUser(user);
        String login_result = managerBean.login();

        RequestDispatcher rd_success = request.getRequestDispatcher("/Index");
        RequestDispatcher rd_failed = request.getRequestDispatcher("LoginPage.jsp");
        if (login_result != null) {
            HttpSession session = request.getSession();
            session.setAttribute("exist_acc", acc);
            if (login_result.equals("ADMIN")) {
                session.setAttribute("is_admin", true);
                rd_success.forward(request, response);
                System.out.println("admin");
            } else {
                session.setAttribute("is_admin", false);
                rd_success.forward(request, response);
                System.out.println("user");
            }
        } else {
            request.setAttribute("msg_failed", "Your account is wrong");
            rd_failed.forward(request, response);
            System.out.println("false");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
