/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webapp.entities;

import java.sql.Timestamp;

/**
 *
 * @author viquy
 */
public class Comment {

    private int id;
    private String username;
    private int postid;
    private String content;
    private String picture;
    private Timestamp date_create;

    public Comment() {
    }

    public Comment(int id, String username, int postid, String content, String picture, Timestamp date_create) {
        this.id = id;
        this.username = username;
        this.postid = postid;
        this.content = content;
        this.picture = picture;
        this.date_create = date_create;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Timestamp getDate_create() {
        return date_create;
    }

    public void setDate_create(Timestamp date_create) {
        this.date_create = date_create;
    }
    
    
}
